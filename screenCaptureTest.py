import pyautogui
import json
import os
import cv2
import datetime
import math
import shutil
import TuxLanesDetector
import time
import subprocess
import threading
import sys


class AutoTux:

    def __init__(self, conf):
        self.conf = conf

    def clear_files(self):
        folders = [self.conf['screenshotsFolder'], self.conf['framesFolder']]
        for folder in folders:
            files = os.listdir(folder)
            for file in files:
                file_path = os.path.join(folder, file)
                try:
                    if os.path.isfile(file_path):
                        os.unlink(file_path)
                except Exception as e:
                    print(e)

    def drive(self, detector):
        if detector:
            pyautogui.keyDown('up')
            direction = detector.direction
            distance = abs(1 - direction.direction) * 0.5
            if direction.direction > 1:
                pyautogui.keyDown('right')
                time.sleep(distance)
                pyautogui.keyUp('right')
            elif direction.direction < 1:
                pyautogui.keyDown('left')
                time.sleep(distance)
                pyautogui.keyUp('left')
            time.sleep(0.5)
            pyautogui.keyUp('up')

    def get_video_name(self):
        # Set video name to first command line argument
        if len(sys.argv) > 1:
            return sys.argv[1]
        # Use date-time if the command line argument is not set
        return datetime.datetime.now().strftime("%Y-%m-%dT%H_%M_%S")
	
    def main(self):
        print("Please switch to super tux kart window")
        time.sleep(2)

        recordFps = self.conf['recordFps']
        frameDuration = recordFps ** -1

        image_folder = 'resources/frames'

        # Clear screenshot folder
        self.clear_files()

        # Recording
        try:
            print(
                "Press Control + C to quit recording. Don't press it twice though as this would exit the whole program!")
            while True:
                conf = self.conf['screenRegion']
                beforeTime = time.time()
                image_name = str(int(round(time.time() * 1000))) + ".png"
                image_path = "resources/screenshots/" + image_name
                pyautogui.screenshot(image_path, region=(conf['x'], conf['y'], conf['width'], conf['height']))
                try:
                    detector = TuxLanesDetector.LanesDetector()
                    detector.transform(image_path, "resources/frames/" + image_name)

                    t1 = threading.Thread(target=self.drive, args=(detector,))
                    t1.daemon = False
                    t1.start()
                except BaseException as e:
                    if isinstance(e, KeyboardInterrupt):
                        raise e  # If is keyboardInterrupt, bubble up

                    print("Transformation error with this file: ", image_name)
                os.unlink(image_path)

                timeToWait = frameDuration - (time.time() - beforeTime)
                time.sleep(timeToWait if timeToWait > 0 else 0)
        except KeyboardInterrupt:
            print("Finished recording. Wait while the video is being rendered.")

        # File directory and name
        video_dir = os.path.abspath('static/' + self.get_video_name())
        video_name = os.path.join(video_dir, 'video.avi')
        os.mkdir(video_dir)

        # Combine images to video
        images = [img for img in os.listdir(image_folder) if img.endswith(".png")]
        frame = cv2.imread(os.path.join(image_folder, images[0]))
        height, width, layers = frame.shape

        four_cc = cv2.VideoWriter_fourcc(*'x264')
        video = cv2.VideoWriter(video_name, 0, recordFps, (width, height))

        for image in images:
            video.write(cv2.imread(os.path.join(image_folder, image)))

        # Thumbnail
        thumbnail_path = os.path.join(image_folder,
                                      images[math.floor(len(images) * 0.5)])  # Get picture in the middle of video
        shutil.copyfile(thumbnail_path, video_dir + '/thumbnail.png')

        # Clear up
        cv2.destroyAllWindows()
        video.release()
        self.clear_files()

        # Video conversion
        args = ["ffmpeg", "-i", video_name, "-b:v", "3M", "-vcodec", "libvpx", os.path.join(video_dir, 'video.webm')]
        subprocess.run(args)
        args = ["ffmpeg", "-i", video_name, "-b:v", "3M", "-vcodec", "libtheora", os.path.join(video_dir, 'video.ogg')]
        subprocess.run(args)
        args = ["ffmpeg", "-i", video_name, "-b:v", "3M", "-vcodec", "libx265", os.path.join(video_dir, 'video.mp4')]
        subprocess.run(args)

        # Remove raw video
        os.unlink(video_name)


if __name__ == '__main__':
    with open("config.json", "r") as file:
        AutoTux(json.loads(file.read())).main()
