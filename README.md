# autotux

## Project Setup Windows

Python Version 3.7.4

Upgrade pip to newest Version (only first time)

```powershell
pip install --upgrade pip
```

Install virtualenv (only first time)

```powershell
pip install virtualenv
```

Create virtual env (only first time)

```powershell
py -m venv venv
```

Deactivate powershell execution policy (only first time)

```powershell
Set-ExecutionPolicy Unrestricted -Scope CurrentUser -Force
```

Activate virtual env

```powershell
. .\venv\Scripts\Activate.ps1
```

Install pip requirements (only first time)

```powershell
pip install -r /path/to/requirements.txt
```


## Project Setup Linux
Install the right python version with apt install.

Install virtualenv:
```bash
sudo apt-get install python-virtualenv
```

Setup virtualenv in the project directory:
```bash
virtualenv --python=python3.7 venv
```

Answer from: https://stackoverflow.com/a/47842394/7130107

## Client-server API

The server generates video files. These static assets will be saved in a directory.  Every video consists out of a directory that contains the different assets. 

Example:

```
static
├───2019-10-18_1
│       thumbnail.jpg
│       video.av1
│       video.mp4
│
└───2019-10-18_2
        thumbnail.jpg
        video.av1
        video.mp4
        
```

The files are then served by a nginx server, with the following configuration:

```nginx
server {
   #[...]
   location /static/ {
       autoindex on;
       autoindex_format json;
   }
   #[...]
}
```

The full configuration can be found in [nginx.conf](nginx.conf). 

## Start Frontend 

To start and develop the frontend application you can simply execute 

```shell script
docker-compose up
```

This will start the nginx server and the vue app with hot module replacement. The first time you start it it will
take a few minutes due to the JS dependencies that have to be installed.

You can access the frontend under <http://localhost/>

# Generate supertuxkart videos
1. Start supertuxkart and start a game
2. Activate the virtual environment as described above or with `source venv/bin/activate` on linux.
3. Run `pip install -r requirements.txt`
3. Edit the values in `config.json` to match your needs
4. Run `python screenCaptureTest.py`
5. Immediately switch to the supertuxkart window
5. To stop, switch back to the console and type `ctrl+c` ONCE.
4. Wait for the encoding to finish
4. Done