import Vue from 'vue';
import App from './App.vue';
import Config from './config.js';

Vue.config.productionTip = false;
Vue.use(Config);

new Vue({
    render: h => h(App),
}).$mount('#app');
