import config from '../autotux.json';

const Config = {
    install(Vue) {
        Vue.prototype.$config = config;
    }
};

export default Config;
